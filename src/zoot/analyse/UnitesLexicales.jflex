package zoot.analyse ;

import java_cup.runtime.*;
import zoot.exceptions.AnalyseLexicaleException;
      
%%
   
%class AnalyseurLexical
%public

%line
%column
    
%type Symbol
%eofval{
        return symbol(CodesLexicaux.EOF) ;
%eofval}

%cup

%{

  private StringBuilder chaine ;

  private Symbol symbol(int type) {
	return new Symbol(type, yyline, yycolumn) ;
  }

  private Symbol symbol(int type, Object value) {
	return new Symbol(type, yyline, yycolumn, value) ;
  }
%}

csteE = [0-9]+
idf = [a-zA-Z_]+[a-zA-Z_]*
finDeLigne = \r|\n
espace = {finDeLigne}  | [ \t\f]

%%
"//".*                 { }

"entier"               { return symbol(CodesLexicaux.ENTIER); }
"booleen"              { return symbol(CodesLexicaux.BOOLEEN); }
"vrai"                 { return symbol(CodesLexicaux.VRAI); }
"faux"                 { return symbol(CodesLexicaux.FAUX); }

"variables"            { return symbol(CodesLexicaux.VARIABLES); }
"fonctions"            { return symbol(CodesLexicaux.FONCTIONS); }
"debut"                { return symbol(CodesLexicaux.DEBUT); }
"fin"              	   { return symbol(CodesLexicaux.FIN); }
"("                    { return symbol(CodesLexicaux.PAR_OPEN); }
")"                    { return symbol(CodesLexicaux.PAR_CLOSE); }
"retourne"             { return symbol(CodesLexicaux.RETOURNE); }

"ecrire"               { return symbol(CodesLexicaux.ECRIRE); }

","                    { return symbol(CodesLexicaux.VIRGULE);}

"="                    { return symbol(CodesLexicaux.EGAL); }

"=="                   { return symbol(CodesLexicaux.CEGAL); }
"!="                   { return symbol(CodesLexicaux.CDIFF); }
"<"                    { return symbol(CodesLexicaux.INF); }

"et"                   { return symbol(CodesLexicaux.ET); }
"ou"                   { return symbol(CodesLexicaux.OU); }
"non"                  { return symbol(CodesLexicaux.NON); }

"+"                    { return symbol(CodesLexicaux.PLUS); }
"*"                    { return symbol(CodesLexicaux.FOIS); }
"-"                    { return symbol(CodesLexicaux.MOINS); }

"si"                   { return symbol(CodesLexicaux.SI); }
"alors"                { return symbol(CodesLexicaux.ALORS); }
"sinon"                { return symbol(CodesLexicaux.SINON); }
"finsi"                { return symbol(CodesLexicaux.FINSI); }

"repeter"              { return symbol(CodesLexicaux.REPETER); }
"jusqua"               { return symbol(CodesLexicaux.JUSQUA); }
"finrepeter"           { return symbol(CodesLexicaux.FINREPETER); }

";"                    { return symbol(CodesLexicaux.POINTVIRGULE); }

{csteE}      	       { return symbol(CodesLexicaux.CSTENTIERE, yytext()); }

{idf}                  { return symbol(CodesLexicaux.IDF, yytext()); }

{espace}               { }
.                      { throw new AnalyseLexicaleException(yyline, yycolumn, yytext()) ; }


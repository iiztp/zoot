package zoot.arbre;

import zoot.arbre.instructions.Instruction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * 21 novembre 2018
 *
 * @author brigitte wrobel-dautcourt
 */

public class BlocDInstructions extends ArbreAbstrait implements Iterable<Instruction> {
    
    protected List<Instruction> bloc = new ArrayList<>();

    public BlocDInstructions(int n) {
        super(n) ;
    }
    
    public void ajouter(Instruction i) {
        if(i != null)
            bloc.add(i) ;
    }

    @Override
    public void verifier() {
        for(Instruction inst : bloc)
        {
            inst.verifier();
        }
    }
    
    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder();

        for(Instruction inst : bloc)
        {
            sb.append(inst.toMIPS());
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return bloc.toString() ;
    }

    @Override
    public Iterator<Instruction> iterator() {
        return bloc.iterator();
    }
}

package zoot.arbre;

import zoot.arbre.instructions.Instruction;
import zoot.arbre.instructions.SousBloc;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;

import java.util.Collection;

public class Fonction extends ArbreAbstrait
{
    protected String nom;
    protected VarType type;
    protected int nbParams;
    protected int nbVar;
    protected BlocDInstructions content;

    public Fonction(VarType type, String nom, int nbParams, int nbVar, BlocDInstructions content, int n) {
        super(n);
        this.nom = nom;
        this.content = content;
        this.nbParams = nbParams;
        this.nbVar = nbVar;
        this.type = type;
    }

    @Override
    public void verifier() {
        boolean hasReturn = false;

        for(Instruction i : content)
        {
            i.setParentFunc(this);
            hasReturn = hasReturn || i.estUnRetourne();
            i.verifier();
            if(i.estUnSousBloc())
            {
                hasReturn = hasReturn || ((SousBloc)i).hasReturn();
            }
        }

        try{
            if(!hasReturn)
                throw new AnalyseSemantiqueException("Fonction : " + nom + ", Retourne manquant", getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
    }

    public String getTag() {
        return nom + nbParams;
    }

    public String getNom() {
        return nom;
    }

    public VarType getType() {
        return type;
    }

    public int getNbParams()  {
        return nbParams;
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder();

        sb.append(getTag()).append(":\n");

        sb.append("\tsw $ra, 12($s7)\n");
        sb.append("\taddi $sp, $sp, ").append(nbVar*(-4)).append("\n");


        for(Instruction i : content) {
            sb.append(i.toMIPS());
        }

        return sb.toString();
    }
}

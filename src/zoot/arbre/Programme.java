package zoot.arbre;

import zoot.variables.GestionnaireTDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Programme extends ArbreAbstrait
{
    protected BlocDInstructions principal;
    protected List<Fonction> fonctions;

    public Programme(BlocDInstructions principal, List<Fonction> fonctions, int n) {
        super(n);
        this.principal = principal;
        this.fonctions = fonctions;
    }

    @Override
    public void verifier() {
        principal.verifier();
        int bloc = 0;
        for(Fonction f : fonctions){
            GestionnaireTDS.getInstance().setBloc(++bloc);
            f.verifier();
        }
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder().append(".data\n")
                .append("new_line: .asciiz \"\\n\"\n")
                .append("true: .asciiz \"vrai\"\n")
                .append("false: .asciiz \"faux\"\n")
                .append("msg_error: .asciiz \"ERREUR EXECUTION\"\n")
                .append(".text\n\n")
                .append("main:\n")
                .append("\tmove $s7, $sp\n")
                .append("\tmove $s1, $s7\n")
                .append("\taddi $sp, $sp, ").append(GestionnaireTDS.getInstance().mainSize()).append("\n");


        GestionnaireTDS.getInstance().setBloc(0);
        sb.append(principal.toMIPS());

        //Fin du programme
        sb.append("end:\n").append("\tli $v0, 10\n").append("\tsyscall\n");

        //Ecrire booleen
        sb.append("ecrire_b:\n");
        sb.append("\tbeqz $a0, ").append("ecrire_b_faux\n");
        sb.append("\tla $a0, true\n\tli $v0, 4\n");
        sb.append("\tj ").append("ecrire_b_fin").append("\n");
        sb.append("\tecrire_b_faux").append(":").append("\n");
        sb.append("\tla $a0, false\n\tli $v0, 4\n");
        sb.append("\tecrire_b_fin").append(":").append("\n");
        sb.append("\tsyscall\n\tla $a0, new_line\n\tli $v0, 4\n\tsyscall\n");
        sb.append("\tjr $ra\n");

        int bloc = 0;
        for(Fonction f : fonctions)
        {
            GestionnaireTDS.getInstance().setBloc(++bloc);
            sb.append(f.toMIPS());
        }

        //Erreur
        sb.append(".ktext 0x80000180\n");
        sb.append("\tla $a0, msg_error\n\tli $v0, 4\n\tsyscall\n");
        sb.append("\tli $v0, 10\n").append("\tsyscall\n");

        return sb.toString();
    }
}

package zoot.arbre.expressions;

import zoot.arbre.ArbreAbstrait;
import zoot.variables.VarType;

public abstract class Expression extends ArbreAbstrait {
    private VarType type;
    protected Expression(int n, VarType type) {
        super(n) ;
        this.type = type;
    }

    public VarType getType() {
        return type;
    }
}

package zoot.arbre.expressions.nterminales;

import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;

import java.util.Collection;
import java.util.Collections;

public abstract class Binaire extends Expression
{
    protected Expression left;
    protected Expression right;

    protected Binaire(Expression left, Expression right, int n, VarType type) {
        super(n, type);
        this.left = left;
        this.right = right;
    }

    @Override
    public void verifier() {
        left.verifier();
        right.verifier();
        try {
            if (left.getType() != getType() || right.getType() != getType())
                throw new AnalyseSemantiqueException("Types des expressions incompatibles, type : " + left.getType() + " " + this + " " + right.getType() + ", attendu : " + getType(), getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String toMIPS() {
        return left.toMIPS() +
                "\tsw $v0, ($sp)\n" +
                "\taddi $sp, $sp, -4\n" +
                right.toMIPS() +
                "\taddi $sp, $sp, 4\n" +
                "\tlw $t8, ($sp)\n";
    }
}

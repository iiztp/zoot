package zoot.arbre.expressions.nterminales;

import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;

public abstract class Unaire extends Expression {
    protected Expression exp;

    protected Unaire(Expression e, int n, VarType type) {
        super(n, type);
        this.exp = e;
    }

    @Override
    public void verifier() {
        exp.verifier();
        try {
            if (!exp.getType().equals(getType()))
                throw new AnalyseSemantiqueException("Types des expressions incompatibles, type : " + exp.getType() + ", attendu : " + getType(), getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
    }

    public void setExp(Expression exp) {
        this.exp = exp;
    }
}

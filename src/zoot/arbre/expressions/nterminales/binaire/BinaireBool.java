package zoot.arbre.expressions.nterminales.binaire;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.Binaire;
import zoot.variables.GestionnaireTDS;
import zoot.variables.VarType;

public abstract class BinaireBool extends Binaire {

    protected final String etiquette = "_" + GestionnaireTDS.getInstance().getCpt();

    protected BinaireBool(Expression l, Expression r, int n) {
        super(l, r, n, VarType.BOOLEEN);
    }

    @Override
    public void verifier()
    {
        left.verifier();
        right.verifier();
    }

    @Override
    public String toMIPS() {
        return super.toMIPS() +
                intraToMIPS() +
                "\tli $v0, 0\n" +
                "\tj " + etiquette + "_end\n" +
                "\t" + etiquette + ":\n" +
                "\tli $v0, 1\n" +
                "\t" + etiquette + "_end:\n";
    }

    protected abstract String intraToMIPS();
}

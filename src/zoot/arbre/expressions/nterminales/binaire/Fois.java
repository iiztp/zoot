package zoot.arbre.expressions.nterminales.binaire;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.Binaire;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;

import java.util.Collection;

public class Fois extends Binaire
{
    public Fois(Expression l, Expression r, int n) {
        super(l, r, n, VarType.ENTIER);
    }

    @Override
    public String toMIPS() {
        return super.toMIPS() +
                "\tmul $v0, $t8, $v0\n";
    }

    @Override
    public String toString()
    {
        return "*";
    }
}

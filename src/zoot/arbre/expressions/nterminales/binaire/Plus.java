package zoot.arbre.expressions.nterminales.binaire;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.Binaire;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;

import java.util.Collection;

public class Plus extends Binaire
{
    public Plus(Expression left, Expression right, int n) {
        super(left, right, n, VarType.ENTIER);
    }

    @Override
    public String toMIPS() {
        return super.toMIPS() +
                "\tadd $v0, $t8, $v0\n";
    }

    @Override
    public String toString()
    {
        return "+";
    }
}

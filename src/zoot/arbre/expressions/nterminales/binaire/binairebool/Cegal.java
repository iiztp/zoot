package zoot.arbre.expressions.nterminales.binaire.binairebool;


import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.binaire.BinaireBool;
import zoot.exceptions.AnalyseSemantiqueException;

import java.util.Collection;

public class Cegal extends BinaireBool
{
    public Cegal(Expression l, Expression r, int n) {
        super(l, r, n);
    }

    @Override
    public void verifier() {
        super.verifier();
        try{
            if(left.getType() != right.getType())
                throw new AnalyseSemantiqueException("Types des expressions incompatibles, type : " + left.getType() + " " + this + " " + right.getType() + ", attendu : " + getType(), getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String intraToMIPS() {
        return "\tbeq $v0, $t8, " + etiquette + "\n";
    }

    @Override
    public String toString()
    {
        return "==";
    }
}

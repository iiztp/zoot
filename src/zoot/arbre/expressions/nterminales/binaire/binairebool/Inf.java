package zoot.arbre.expressions.nterminales.binaire.binairebool;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.binaire.BinaireBool;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;

import java.util.Collection;

public class Inf extends BinaireBool
{
    public Inf(Expression l, Expression r, int n) {
        super(l, r, n);
    }

    @Override
    public void verifier() {
        super.verifier();

        try{
            if(left.getType() != VarType.ENTIER || right.getType() != VarType.ENTIER)
                throw new AnalyseSemantiqueException("Types des expressions incompatibles, type : " + left.getType() + " " + this + " " + right.getType() + ", attendu : " + getType(), getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String intraToMIPS() {
        return  "\tsub $v0, $t8, $v0\n" +
                "\tbltz $v0, " + etiquette + "\n";
    }

    @Override
    public String toString()
    {
        return "<";
    }
}

package zoot.arbre.expressions.nterminales.unaire;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.Unaire;
import zoot.variables.VarType;

import java.util.Collection;

public class Moins extends Unaire
{
    public Moins(Expression e, int n) {
        super(e, n, VarType.ENTIER);
    }

    @Override
    public String toMIPS() {
        return exp.toMIPS() +
                "\tnegu $v0, $v0\n";
    }
}

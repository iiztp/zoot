package zoot.arbre.expressions.nterminales.unaire;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.nterminales.Unaire;
import zoot.variables.VarType;

import java.util.Collection;

public class Non extends Unaire
{
    public Non(Expression e, int n) {
        super(e, n, VarType.BOOLEEN);
    }

    @Override
    public String toMIPS() {
        return exp.toMIPS() +
                "addi $v0, $v0, 1\n" +
                "andi $v0, $v0, 1\n";
    }
}

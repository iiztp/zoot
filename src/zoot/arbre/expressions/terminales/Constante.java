package zoot.arbre.expressions.terminales;

import zoot.arbre.expressions.Expression;
import zoot.variables.VarType;

public abstract class Constante extends Expression {

    protected String cste ;
    
    protected Constante(String texte, int n, VarType type) {
        super(n, type) ;
        cste = texte ;
    }
    
    @Override
    public void verifier() {

    }

    @Override
    public String toString() {
        return cste ;
    }
}

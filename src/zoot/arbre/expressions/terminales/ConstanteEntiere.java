package zoot.arbre.expressions.terminales;

import zoot.variables.VarType;

import java.util.Collection;

public class ConstanteEntiere extends Constante {
    
    public ConstanteEntiere(String texte, int n) {
        super(texte, n, VarType.ENTIER) ;
    }

    @Override
    public String toMIPS() {
        return "\tli $v0, " + this + "\n";
    }
}

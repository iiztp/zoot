package zoot.arbre.expressions.terminales;

import zoot.variables.VarType;

import java.util.Collection;

public class Faux extends Constante
{

    public Faux(int n) {
        super("faux", n, VarType.BOOLEEN);
    }

    @Override
    public String toMIPS() {
        return "\tli $v0, 0\n";
    }
}

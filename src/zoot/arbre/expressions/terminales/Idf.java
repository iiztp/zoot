package zoot.arbre.expressions.terminales;

import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.GestionnaireTDS;
import zoot.variables.symbols.Symbol;
import zoot.variables.VarType;
import zoot.variables.entries.Entree;

public abstract class Idf extends Expression
{
    protected String name ;
    protected Symbol symbol;

    public Idf(String name, int n) {
        super(n, null);
        this.name = name;
    }

    public void verifier(Entree entree)
    {
        try {
            this.symbol = GestionnaireTDS.getInstance().getSymbol(entree);
        }
        catch(AnalyseSemantiqueException e)
        {
            throw new AnalyseSemantiqueException(e.getMessage(), noLigne);
        }
    }

    @Override
    public VarType getType()
    {
        return symbol.getType();
    }

    public int getDeplacement() {
        return symbol.getDeplacement();
    }

    public Symbol getSymbol() {
        return symbol;
    }

    @Override
    public String toString() { return name; }

    @Override
    public String toMIPS() {
        return null;
    }
}

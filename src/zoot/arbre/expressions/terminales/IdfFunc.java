package zoot.arbre.expressions.terminales;

import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.VarType;
import zoot.variables.entries.EntreeFonction;
import zoot.variables.symbols.SymbolFunc;

import java.util.Collection;
import java.util.List;

public class IdfFunc extends Idf {
    List<Expression> params;
    public IdfFunc(String name, List<Expression> params, int n) {
        super(name, n);
        this.params = params;
    }

    @Override
    public void verifier() {
        StringBuilder types = new StringBuilder();
        for(Expression param : params) {
            try {
                param.verifier();
                types.append(param.getType()).append(" ");
            } catch (AnalyseSemantiqueException e) {
                System.err.println(e.getMessage());
            }
        }
        try {
            verifier(new EntreeFonction(name, params.size()));
            List<VarType> funcParams = ((SymbolFunc) symbol).getParameters();
            for(int i = 0; i < params.size(); i++)
            {
                if(!params.get(i).getType().isCompatibleWith(funcParams.get(i)))
                    throw new AnalyseSemantiqueException("Fonction " + name + "( " + types + ") inconnue !", getNoLigne());
            }
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
        catch(ClassCastException ignored){}
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder();

        sb.append("\taddi $sp, $sp, -12\n");
        sb.append("\tsw $s7, 4($sp)\n");

        for(int i = 0; i < params.size(); i++) {
            sb.append(params.get(i).toMIPS());
            sb.append("\tsw $v0, ").append((i*(-4))).append("($sp)\n");
        }

        sb.append("\tmove $s7, $sp\n");
        sb.append("\taddi $sp, $sp, ").append((params.size()*(-4))).append("\n");

        sb.append("\tjal ").append(name).append(params.size()).append("\n");
        sb.append("\tmove $t1, $s7\n");
        sb.append("\taddi $t1, $t1, 12\n");
        sb.append("\tmove $sp, $t1\n");
        sb.append("\tlw $s7, 4($s7)\n");

        return  sb.toString();
    }
}

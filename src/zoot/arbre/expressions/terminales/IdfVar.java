package zoot.arbre.expressions.terminales;

import zoot.variables.GestionnaireTDS;
import zoot.variables.VarType;
import zoot.variables.entries.EntreeVariable;
import zoot.variables.symbols.SymbolVar;

import java.util.Collection;

public class IdfVar extends Idf
{
    public IdfVar(String name, int n) {
        super(name, n);
    }

    @Override
    public void verifier() {
        verifier(new EntreeVariable(name));
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder();

        boolean inBloc = ((SymbolVar) symbol).getBloc() != GestionnaireTDS.getInstance().getBloc();
        if(inBloc) {
            sb.append("\tmove $s2, $s7\n");
            sb.append("\tmove $s7, $s1\n");
        }

        if(getType() == VarType.ENTIER) {
            sb.append("\tlw $v0, ");
        } else {
            sb.append("\tlb $v0, ");
        }

        sb.append(symbol.getDeplacement()).append("($s7)\n");

        if(inBloc) {
            sb.append("\tmove $s7, $s2\n");
        }

        return sb.toString();
    }
}

package zoot.arbre.expressions.terminales;

import zoot.variables.VarType;

import java.util.Collection;

public class Vrai extends Constante
{
    public Vrai(int n) {
        super("vrai", n, VarType.BOOLEEN);
    }

    @Override
    public String toMIPS() {
        return "\tli $v0, 1\n";
    }
}

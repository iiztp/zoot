package zoot.arbre.instructions;

import zoot.arbre.expressions.Expression;
import zoot.arbre.expressions.terminales.IdfVar;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.GestionnaireTDS;
import zoot.variables.VarType;
import zoot.variables.symbols.SymbolVar;

import java.util.Collection;

public class Affectation extends Instruction
{
    private final IdfVar var;

    public Affectation(IdfVar var, Expression assign, int n) {
        super(n);
        this.var = var;
        this.exp = assign;
    }

    @Override
    public void verifier() {
        try
        {
            var.verifier();
            exp.verifier();
            if(!var.getType().isCompatibleWith(exp.getType()))
            {
                throw new AnalyseSemantiqueException("Types de variables incompatibles entre '" + var + "' et '" + exp + "'", getNoLigne());
            }
        } catch(AnalyseSemantiqueException e) {
            System.err.println(e.getMessage());
        }
        catch(NullPointerException ignored){}
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder(exp.toMIPS());

        boolean inBloc = ((SymbolVar) var.getSymbol()).getBloc() != GestionnaireTDS.getInstance().getBloc();

        if(inBloc) {
            sb.append("move $s2, $s7\n");
            sb.append("move $s7, $s1\n");
        }

        if(var.getType() == VarType.ENTIER) {
            sb.append("\tsw $v0, ");
        } else {
            sb.append("\tsb $v0, ");
        }

        sb.append(var.getDeplacement()).append("($s7)").append("\n");

        if(inBloc) {
            sb.append("move $s7, $s2\n");
        }

        return sb.toString();
    }
}

package zoot.arbre.instructions;

import zoot.arbre.BlocDInstructions;
import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.GestionnaireTDS;
import zoot.variables.VarType;

import java.util.Collection;

public class Boucle extends SousBloc
{
    private BlocDInstructions content;
    private String etiquette = "_" + GestionnaireTDS.getInstance().getCpt();

    public Boucle(Expression e, BlocDInstructions content, int n) {
        super(n);
        this.exp = e;
        this.content = content;
    }

    @Override
    public void verifier() {
        try
        {
            exp.verifier();
            if(exp.getType() != VarType.BOOLEEN)
                throw new AnalyseSemantiqueException("Expression non booleenne", getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
        for(Instruction i : content)
        {
            hasReturn = hasReturn || i.estUnRetourne();
            i.setParentFunc(parentFunc);
            i.verifier();
            if(i.estUnSousBloc())
            {
                hasReturn = hasReturn || ((SousBloc)i).hasReturn;
            }
        }
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder();

        sb.append("\t").append(etiquette).append(":\n");

        for(Instruction i : content) {
            sb.append(i.toMIPS());
        }

        sb.append(exp.toMIPS());
        sb.append("\tbeqz $v0, ").append(etiquette).append("\n");

        return sb.toString();
    }
}

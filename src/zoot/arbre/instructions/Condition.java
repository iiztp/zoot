package zoot.arbre.instructions;

import zoot.arbre.BlocDInstructions;
import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.GestionnaireTDS;
import zoot.variables.VarType;

import java.util.Collection;

public class Condition extends SousBloc
{
    private final BlocDInstructions ifBloc;
    private final BlocDInstructions elseBloc;
    private final String etiquette = "_" + GestionnaireTDS.getInstance().getCpt();

    public Condition(Expression e, BlocDInstructions si, BlocDInstructions sinon, int n) {
        super(n);
        this.exp = e;
        this.ifBloc = si;
        this.elseBloc = sinon;
    }

    @Override
    public void verifier() {
        try {
            exp.verifier();
            if (exp.getType() != VarType.BOOLEEN)
                throw new AnalyseSemantiqueException("Expression non booleenne", getNoLigne());
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
        boolean hasReturnInIfBloc = false;
        boolean hasReturnInElseBloc = false;
        for(Instruction i : ifBloc)
        {
            i.setParentFunc(parentFunc);
            hasReturnInIfBloc = hasReturnInIfBloc || i.estUnRetourne();
            i.verifier();
            if(parentFunc != null && i.estUnSousBloc())
            {
                hasReturnInIfBloc = hasReturnInIfBloc || ((SousBloc)i).hasReturn;
            }
        }
        for(Instruction i : elseBloc)
        {
            i.setParentFunc(parentFunc);
            hasReturnInElseBloc = hasReturnInElseBloc || i.estUnRetourne();
            i.verifier();
            if(parentFunc != null && i.estUnSousBloc())
            {
                hasReturnInElseBloc = hasReturnInElseBloc || ((SousBloc)i).hasReturn;
            }
        }

        hasReturn = hasReturnInIfBloc && hasReturnInElseBloc;
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder();

        sb.append(exp.toMIPS());
        sb.append("\tbeqz $v0, sinon").append(etiquette).append("\n");

        for(Instruction i : ifBloc) {
            sb.append(i.toMIPS());
        }

        sb.append("\tj finsi").append(etiquette).append("\n");
        sb.append("\tsinon").append(etiquette).append(":\n");

        for(Instruction i : elseBloc) {
            sb.append(i.toMIPS());
        }

        sb.append("\tfinsi").append(etiquette).append(":\n");

        return sb.toString();
    }
}

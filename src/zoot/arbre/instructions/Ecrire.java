package zoot.arbre.instructions;

import zoot.arbre.expressions.Expression;
import zoot.variables.VarType;

import java.util.Collection;

public class Ecrire extends Instruction {
    public Ecrire (Expression e, int n) {
        super(n) ;
        exp = e ;
    }

    @Override
    public void verifier() {
        exp.verifier();
    }

    @Override
    public String toMIPS() {
        StringBuilder sb = new StringBuilder(exp.toMIPS());

        if(exp.getType() == VarType.ENTIER) {
            sb.append("\tmove $a0, $v0\n\tli $v0, 1\n\tsyscall\n\tla $a0, new_line\n\tli $v0, 4\n\tsyscall\n");
        } else {
            sb.append("\tmove $a0, $v0\n");
            sb.append("\tjal ecrire_b\n");
        }

        return sb.toString();

    }
}

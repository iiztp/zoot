package zoot.arbre.instructions;

import zoot.arbre.ArbreAbstrait;
import zoot.arbre.Fonction;
import zoot.arbre.expressions.Expression;

public abstract class Instruction extends ArbreAbstrait {

    protected Fonction parentFunc = null;
    protected Expression exp;

    protected Instruction(int n) {
        super(n);
    }

    public void setParentFunc(Fonction func)
    {
        this.parentFunc = func;
    }

    public boolean estUnSousBloc()
    {
        return false;
    }

    public boolean estUnRetourne()
    {
        return false;
    }
}

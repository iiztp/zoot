package zoot.arbre.instructions;

import zoot.arbre.expressions.Expression;
import zoot.exceptions.AnalyseSemantiqueException;

public class Retourne extends Instruction
{
    public Retourne(Expression e, int n) {
        super(n);
        this.exp = e;
    }

    @Override
    public void verifier() {
        try {
            exp.verifier();
            if(parentFunc == null){
                throw new AnalyseSemantiqueException("'retourne' inattendu dans le bloc principal", getNoLigne());
            }
            if (!parentFunc.getType().isCompatibleWith(exp.getType())) {
                throw new AnalyseSemantiqueException("Fonction : " + parentFunc.getNom() + ", Types incompatibles entre le retour et le type de fonction", getNoLigne());
            }
        }
        catch(AnalyseSemantiqueException e)
        {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public boolean estUnRetourne()
    {
        return true;
    }

    @Override
    public String toMIPS() {
        return exp.toMIPS() +
                "\tsw $v0, 8($s7)\n" +
                "\tlw $ra, 12($s7)\n" +
                "\tjr $ra\n";
    }
}

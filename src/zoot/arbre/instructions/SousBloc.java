package zoot.arbre.instructions;

public abstract class SousBloc extends Instruction
{
    protected boolean hasReturn = false;
    protected SousBloc(int n) {
        super(n);
    }

    @Override
    public boolean estUnSousBloc()
    {
        return true;
    }

    public boolean hasReturn()
    {
        return hasReturn;
    }
}

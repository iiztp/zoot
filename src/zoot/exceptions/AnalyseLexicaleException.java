package zoot.exceptions;

import zoot.utils.ConsoleColor;

public class AnalyseLexicaleException extends RuntimeException {
 
    public AnalyseLexicaleException(int ligne, int colonne, String m) {
        super(ConsoleColor.RED + "ERREUR LEXICALE" + ConsoleColor.RESET + " Ligne " + ligne + ", Colonne " + colonne + " Caractère '" + m + "' non reconnu") ;
    }

}

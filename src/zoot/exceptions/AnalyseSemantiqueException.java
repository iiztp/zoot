package zoot.exceptions;

import zoot.Zoot;
import zoot.utils.ConsoleColor;

public class AnalyseSemantiqueException extends RuntimeException {
    public AnalyseSemantiqueException(String m, int noligne) {
        super(ConsoleColor.RED + "ERREUR SEMANTIQUE" + ConsoleColor.RESET + " Ligne " + noligne + " " + m);
        Zoot.compilationOk = false;
    }

    public AnalyseSemantiqueException(String m) {
        super(m);
    }
}

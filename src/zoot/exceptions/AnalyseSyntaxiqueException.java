package zoot.exceptions;

import zoot.utils.ConsoleColor;

public class AnalyseSyntaxiqueException extends RuntimeException {
 
    public AnalyseSyntaxiqueException(String m, int noligne) {
        super(ConsoleColor.RED + "ERREUR SYNTAXIQUE" + ConsoleColor.RESET + " Ligne " + noligne + " " + m) ;
    }

    public AnalyseSyntaxiqueException(String m) {
        super(ConsoleColor.RED + "ERREUR SYNTAXIQUE" + ConsoleColor.RESET + m) ;
    }

}

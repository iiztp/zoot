package zoot.variables;

import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.entries.Entree;
import zoot.variables.symbols.Symbol;
import zoot.variables.symbols.SymbolVar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GestionnaireTDS
{
    private static final GestionnaireTDS table = new GestionnaireTDS();
    private final List<TableDesSymboles> tdsList = new ArrayList<>();
    private int actualBloc = 0;
    private int cpt = 0;
    private Collection<String> registres = new ArrayList<>();

    private GestionnaireTDS(){
        tdsList.add(new TableDesSymboles());
        for(int i = 0; i < 5; i++)
        {
            registres.add("$t"+i);
        }
    }

    public Collection<String> getRegistres() {
        return registres;
    }

    public void entreeBloc()
    {
        tdsList.add(new TableDesSymboles());
        actualBloc = tdsList.size()-1;
    }

    public void sortieBloc()
    {
        actualBloc = 0;
    }

    public void setBloc(int bloc)
    {
        actualBloc = bloc;
    }

    public int getBloc() {
        return actualBloc;
    }

    public int getCpt() {
        return cpt++;
    }

    public Symbol getSymbol(Entree entree) throws AnalyseSemantiqueException
    {
        Symbol ret;
        try{
            ret = tdsList.get(actualBloc).getSymbol(entree);
        }
        catch(AnalyseSemantiqueException e)
        {
            ret = tdsList.get(0).getSymbol(entree);
        }
        if(ret.estUneFonction() != entree.estUneFonction())
            throw new AnalyseSemantiqueException(entree.getNom() + " est une " + entree.getType() + " mais devrait être une " + ret.getObjectType());
        return ret;
    }

    public void ajouterSymbol(Entree entree, Symbol s) throws AnalyseSemantiqueException
    {
        if(!entree.estUneFonction())
            ((SymbolVar)s).setBloc(actualBloc);
        tdsList.get(actualBloc).ajouterSymbol(entree, s);
    }

    public int size()
    {
        return tdsList.stream().mapToInt(TableDesSymboles::size).sum();
    }

    public int mainSize() {
        return tdsList.get(0).size();
    }

    public static GestionnaireTDS getInstance(){ return table; }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for(TableDesSymboles tds : tdsList)
        {
            sb.append("Bloc ").append(i++).append(" :\n");
            sb.append(tds.toString());
            sb.append("\n");
        }

        return sb.toString();
    }
}

package zoot.variables;

import zoot.exceptions.AnalyseSemantiqueException;
import zoot.variables.entries.Entree;
import zoot.variables.symbols.Symbol;

import java.util.HashMap;
import java.util.Map;

public class TableDesSymboles
{
    private final Map<Entree, Symbol> tds = new HashMap<>();
    private int deplacementCourant = 0;

    protected TableDesSymboles(){}

    public void ajouterSymbol(Entree entree, Symbol s) throws AnalyseSemantiqueException
    {
        if(tds.containsKey(entree)) throw new AnalyseSemantiqueException("Double déclaration de la " + entree + " !", s.getNoligne());
        s.setDeplacement(deplacementCourant);
        tds.put(entree, s);
        if(!entree.estUneFonction())
            deplacementCourant += s.getType().getCapacity();
    }

    public Symbol getSymbol(Entree entree) throws AnalyseSemantiqueException
    {
        if (!tds.containsKey(entree)) throw new AnalyseSemantiqueException(entree + " non déclarée !");
        return tds.get(entree);
    }

    public int size()
    {
        return deplacementCourant;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Name of var\tVar type\tDeplacement\n");
        sb.append("-----------------------------------------------\n");
        tds.forEach((k, v) -> sb.append(k).append("\t").append(v).append("\n"));
        sb.append("-----------------------------------------------\n");

        return sb.toString();
    }
}

package zoot.variables;

public enum VarType
{
    ENTIER(-4),
    BOOLEEN(-4);

    private int capacity;

    VarType(int capacity)
    {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public String toString()
    {
        return this.name();
    }

    public boolean isCompatibleWith(VarType type)
    {
        return this == type;
    }
}

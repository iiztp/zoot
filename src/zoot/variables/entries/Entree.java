package zoot.variables.entries;

public abstract class Entree
{
    private String nom;

    public Entree(String nom)
    {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public abstract String getType();

    @Override
    public int hashCode()
    {
        return 0;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        Entree that = (Entree)obj;
        return getNom().equals(that.getNom());
    }

    public boolean estUneFonction()
    {
        return false;
    }

    @Override
    public String toString()
    {
        return getType() + " " + nom;
    }
}

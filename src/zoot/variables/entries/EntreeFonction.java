package zoot.variables.entries;

public class EntreeFonction extends Entree
{
    private int nbParameters;

    public EntreeFonction(String nom, int nbParameters)
    {
        super(nom);
        this.nbParameters = nbParameters;
    }

    @Override
    public String getType()
    {
        return "Fonction";
    }

    @Override
    public boolean estUneFonction()
    {
        return true;
    }

    @Override
    public boolean equals(Object e)
    {
        if (this == e) return true;
        if (e == null || getClass() != e.getClass()) return super.equals(e);
        EntreeFonction that = (EntreeFonction)e;
        if(!getNom().equals(that.getNom()))
            return false;
        return (nbParameters == that.nbParameters);
    }

    @Override
    public String toString()
    {
        return getType() + " " + getNom() + "( " + nbParameters + "param(s) )";
    }
}

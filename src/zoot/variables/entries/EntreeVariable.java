package zoot.variables.entries;

public class EntreeVariable extends Entree
{
    public EntreeVariable(String nom)
    {
        super(nom);
    }

    @Override
    public String getType()
    {
        return "Variable";
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        Entree that = (Entree)obj;
        return getNom().equals(that.getNom());
    }
}

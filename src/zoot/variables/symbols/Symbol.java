package zoot.variables.symbols;

import zoot.variables.VarType;

public abstract class Symbol
{
    private VarType type;
    private int deplacement = 1;
    private final int noligne;

    public Symbol(VarType type, int noligne)
    {
        this.type = type;
        this.noligne = noligne;
    }

    public void setDeplacement(int deplacement) {
        this.deplacement = deplacement;
    }

    public String getObjectType()
    {
        return "Variable";
    }

    public boolean estUneFonction()
    {
        return false;
    }

    public int getDeplacement() {
        return deplacement;
    }

    public int getNoligne() {
        return noligne;
    }

    public VarType getType() {
        return type;
    }

    public String toString()
    {
        return type + "\t\t" + deplacement;
    }
}

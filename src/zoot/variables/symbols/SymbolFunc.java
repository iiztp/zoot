package zoot.variables.symbols;

import zoot.variables.VarType;

import java.util.List;

public class SymbolFunc extends Symbol
{
    private List<VarType> parameters;

    public SymbolFunc(VarType type, int noligne, List<VarType> params) {
        super(type, noligne);
        this.parameters = params;
    }

    @Override
    public String getObjectType()
    {
        return "Fonction";
    }

    public boolean estUneFonction()
    {
        return true;
    }

    public List<VarType> getParameters() {
        return parameters;
    }
}

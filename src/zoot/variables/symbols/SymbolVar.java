package zoot.variables.symbols;

import zoot.variables.VarType;

public class SymbolVar extends Symbol
{
    private int bloc = 0;

    public SymbolVar(VarType type, int noligne) {
        super(type, noligne);
    }

    public int getBloc() {
        return bloc;
    }

    public void setBloc(int bloc)
    {
        this.bloc = bloc;
    }
}
